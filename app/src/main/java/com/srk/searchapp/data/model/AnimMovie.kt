package com.srk.searchapp.data.model

import java.util.*

class AnimMovie {
    var mal_id = 0
    var url: String? = null
    var image_url: String? = null
    var title: String? = null
    var airing = false
    var synopsis: String? = null
    var type: String? = null
    var episodes = 0
    var score = 0.0
    var start_date: Date? = null
    var end_date: Date? = null
    var members = 0
    var rated: String? = null
}
package com.srk.searchapp.data.repo

import com.srk.searchapp.data.model.MovieSearchResult
import com.srk.searchapp.data.network.SearchApi
import retrofit2.Call
import retrofit2.Response

class DataRepository(val searchApi: SearchApi) {

    fun getAnimMovieList(onMoiveListResult: OnMoiveListResult, searchstring: String) =
        searchApi.getMovieListBySearchString(searchstring = searchstring).enqueue(object : retrofit2.Callback<MovieSearchResult> {

            override fun onResponse(
                call: Call<MovieSearchResult>,
                response: Response<MovieSearchResult>
            ) {
                onMoiveListResult.onSuccess((response.body() as MovieSearchResult?))
            }

            override fun onFailure(call: Call<MovieSearchResult>, t: Throwable) {
                onMoiveListResult.onFailure()
            }
        })

    interface OnMoiveListResult {
        fun onSuccess(data: MovieSearchResult?)
        fun onFailure()
    }
}

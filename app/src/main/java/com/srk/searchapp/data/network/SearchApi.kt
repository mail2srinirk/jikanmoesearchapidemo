package com.srk.searchapp.data.network

import com.srk.searchapp.data.model.MovieSearchResult
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryName

interface SearchApi {

    @GET("/v3/search/anime")
    fun getMovieListBySearchString(@Query("q") searchstring: String): Call<MovieSearchResult>
}
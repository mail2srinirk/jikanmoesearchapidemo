package com.srk.searchapp.data.model

class MovieSearchResult {
    var request_hash: String? = null
    var request_cached = false
    var request_cache_expiry = 0
    var results: List<AnimMovie>? = null
    var last_page = 0
}
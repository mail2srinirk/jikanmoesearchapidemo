package com.srk.searchapp.view.activity

import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.srk.searchapp.R
import com.srk.searchapp.data.model.AnimMovie
import com.srk.searchapp.data.model.MovieSearchResult
import com.srk.searchapp.data.network.createWebService
import com.srk.searchapp.data.repo.DataRepository
import com.srk.searchapp.view.adapter.MovieListAdapter
import com.srk.searchapp.viewmodel.AnimationMoviewViewModel
import com.srk.searchapp.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_searchresult_movielist.*

class SearchResults : AppCompatActivity() {

    private val repository: DataRepository = DataRepository(createWebService())
    private lateinit var movieListViewModel: AnimationMoviewViewModel

    private lateinit var progressBar: ProgressBar
    private var searchstring: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchresult_movielist)
        progressBar = findViewById(R.id.progressBar) as ProgressBar
        val bundle = intent.extras
        bundle!!.getString("searchstring")?.let {
            searchstring = it}
        movieListViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(repository)
        ).get(AnimationMoviewViewModel::class.java)

        movieListViewModel.setListener(object: AnimationMoviewViewModel.ModelStatusListener{
            override fun onSuccess(data: MovieSearchResult?) {
                progressBar.visibility = View.GONE
                if(data?.results.isNullOrEmpty()){
                    val intent = Intent(applicationContext, SearchFailed::class.java)
                    startActivity(intent)
                }
            }

            override fun onFailure() {
                progressBar.visibility = View.GONE
                val intent = Intent(applicationContext, SearchFailed::class.java)
                startActivity(intent)
            }

        })
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        // your code to perform when the user clicks on the button
        //Toast.makeText(this@MainActivity, "You clicked me.", Toast.LENGTH_SHORT).show()
        movielist!!.layoutManager = LinearLayoutManager(
            movielist!!.context,
            RecyclerView.VERTICAL,
            false
        )
        progressBar.visibility = View.VISIBLE
        movieListViewModel.getAnimMovieList(searchstring!!)
        movieListViewModel.MoviesList.observe(
            this,
            Observer(function = fun(animMovielist: List<AnimMovie>?) {
                animMovielist?.let {
                    val movieListAdapter =
                        MovieListAdapter(
                            animMovielist
                        )
                    movielist.adapter = movieListAdapter
                } ?: run {
                    progressBar.visibility = View.GONE
                    val intent = Intent(this, SearchFailed::class.java)
                    startActivity(intent)
                }
            } )
        )

    }

}
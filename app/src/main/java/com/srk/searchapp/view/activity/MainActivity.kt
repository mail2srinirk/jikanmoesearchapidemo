package com.srk.searchapp.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.srk.searchapp.R

class MainActivity : AppCompatActivity() {
    private lateinit var searchbtn: Button
    private lateinit var searchtextinput: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // get reference to button
        searchbtn = findViewById(R.id.button) as Button
        searchtextinput = findViewById(R.id.txtsearchstr) as EditText
        // set on-click listener
        searchbtn?.setOnClickListener {
            var searchsgtring: String = searchtextinput.text.toString()
            val intent = Intent(this, SearchResults::class.java)
            intent.putExtra("searchstring", searchsgtring)
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            startActivity(intent)
        }
    }
}
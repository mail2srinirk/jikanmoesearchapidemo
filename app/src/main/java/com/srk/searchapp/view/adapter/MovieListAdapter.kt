package com.srk.searchapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.srk.searchapp.R
import com.srk.searchapp.data.model.AnimMovie
import kotlinx.android.synthetic.main.adapter_movie_list_item.view.*

class MovieListAdapter(private val movieList: List<AnimMovie>?) :
    RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.adapter_movie_list_item, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return movieList?.size!!
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        movieList?.let {
            viewHolder.title.text = it.get(position)?.title
            viewHolder.summary.text = it.get(position)?.synopsis
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.moviename
        val summary: TextView = itemView.moviedetails
    }
}
package com.srk.searchapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.srk.searchapp.data.model.AnimMovie
import com.srk.searchapp.data.model.MovieSearchResult
import com.srk.searchapp.data.repo.DataRepository

class AnimationMoviewViewModel(val dataRepository: DataRepository) : ViewModel() {

    var MoviesList = MutableLiveData<List<AnimMovie>>()
    var modelStatusListener: ModelStatusListener? = null

    public interface ModelStatusListener{
        fun onSuccess(data: MovieSearchResult?)
        fun onFailure()
    }
    init {
        MoviesList.value = listOf()
    }

    fun setListener(searchResultListener: ModelStatusListener){
        modelStatusListener = searchResultListener
    }

    fun getAnimMovieList(searchstring: String) {
        dataRepository.getAnimMovieList( object : DataRepository.OnMoiveListResult {
            override fun onSuccess(data: MovieSearchResult?) {
                MoviesList.value = data?.results
                modelStatusListener?.onSuccess(data)
            }

            override fun onFailure() {
                //REQUEST FAILED
                MoviesList.value =  null
                modelStatusListener?.onFailure()
            }
        },searchstring)
    }
}
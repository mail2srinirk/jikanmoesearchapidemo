package com.srk.searchapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.srk.searchapp.data.repo.DataRepository

class ViewModelFactory(val repository: DataRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass === AnimationMoviewViewModel::class.java)
            return AnimationMoviewViewModel(repository) as T
        return super.create(modelClass)
    }
}